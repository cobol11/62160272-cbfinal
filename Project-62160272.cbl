       IDENTIFICATION DIVISION. 
       PROGRAM-ID. FINAL-PROJECT.
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT DATA-INPUT ASSIGN TO "trader2.dat" 
              ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD DATA-INPUT.
       01  DATA-REC.
           88 END-OF-DATA-FILE VALUE HIGH-VALUE. 
           05 CITY-CODE PIC 99.
           05 ID-TRADER PIC 9(4).
           05 INCOME PIC 9(6). 
       WORKING-STORAGE SECTION.
       01  REPORT-HEADER01 PIC X(50)
           VALUE "PROVINCE    P INCOME    MEMBER  MEMBER INCOME".
       01  DETAIL-LINE.
           05 PRN-CITY-CODE PIC 99.
           05 PRN-INCOME-CITY PIC 9(15).
           05 PRN-ID-TRADER PIC 9(4).
           05 PRN-INCOME-TRADER PIC 9(7).
       01  DATA-TRADER-TABLE OCCURS 1000 TIMES.
           05 DATA-CITY-TOTAL PIC 9(15).
           05 CITY-BRANCE PIC 99.
       01  CITY-IDX PIC 9(4).

       PROCEDURE DIVISION.
       BEGIN.
           MOVE ZEROS TO DATA-TRADER-TABLE
           OPEN INPUT DATA-INPUT
           READ DATA-INPUT
              AT END SET END-OF-DATA-FILE TO TRUE
           END-READ
           PERFORM UNTIL END-OF-DATA-FILE 
              PERFORM READ-FILE
           END-PERFORM
           PERFORM PRINT-RESULTS 

           CLOSE DATA-INPUT
           STOP RUN
           .
       PRINT-RESULTS.
           DISPLAY REPORT-HEADER01
           PERFORM VARYING CITY-IDX FROM 1 BY 1
                 UNTIL CITY-IDX GREATER THAN 1000
           END-PERFORM
      *    COMPUTE PRN-INCOME-CITY =  
           .
       READ-FILE.
           READ DATA-INPUT
              AT END SET END-OF-DATA-FILE TO TRUE
           END-READ
           .
